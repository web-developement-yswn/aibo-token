import bg2 from "../static/images/bg2.jpg"
import bg3 from "../static/images/bg3.jpg"
import bg4 from "../static/images/bg4.jpg"
import logo from "../static/images/logo.png"
import facebook from "../static/images/facebook.svg"
import twitter from "../static/images/twitter.svg"
import telegram from "../static/images/telegram.svg"

import corgi from "../static/images/corgi.png"

const images_list = {
	bg2,
	bg3,
	bg4,
	logo,
	facebook,
	twitter,
	telegram,

	corgi
}

export default images_list
