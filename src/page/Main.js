import { useState, useRef } from "react"
import styled from "styled-components"
import image from "../constant/ImportImages"

//components
import Navbar from "../component/Navbar"
import AboutUs from "../component/AboutUs"
import Tokenomic from "../component/Tokenomic"
import Stats from "../component/Stats"
import Roadmap from "../component/Roadmap"
import Footer from "../component/Footer"

const App = () => {
	const [copy, setCopy] = useState("Copy")
	const ref = useRef(null)
	const handleCopy = (e) => {
		ref.current.select()
		document.execCommand("copy")
		setCopy("Copied!")
	}
	return (
		<MainStyles>
			<Navbar />
			<div className="hero container">
				<div className="hero__left">
					<div className="head">
						<h1 className="head__title">
							<span> AIBO</span>
							TOKEN
						</h1>
						<div className="head__subtitle">Lorem ipsum dolor sit amet</div>
					</div>
					<div className="copy">
						<input
							type="text"
							id="fname"
							name="fname"
							defaultValue="0x737f0e47c4d4167a3eecde5fa87306b6eee3140e"
							readOnly
							ref={ref}
						/>
						<div className="copy__btn" onClick={() => handleCopy()}>
							<span>{copy}</span>
						</div>
					</div>
					<div className="btngroup">
						<button className="btngroup__btn">How to Buy</button>
						<button className="btngroup__btn">Donate</button>
					</div>
					<div className="social">
						<div className="social__subtitle">Follow us on</div>
						<div className="social__icon">
							<a href="#aa" target="__blank">
								<img src={image.facebook} alt="fb" />
							</a>
							<a href="#aa" target="__blank">
								<img src={image.twitter} alt="tw" />
							</a>
							<a href="#aa" target="__blank">
								<img src={image.telegram} alt="tg" />
							</a>
						</div>
					</div>
				</div>
				<div className="hero__right">
					<img src={image.corgi} alt="" className="hero__logo" />
				</div>
			</div>
			<div className="bg">
				<AboutUs />
				<Tokenomic />
				<Stats />
				<Roadmap />
				<Footer />
			</div>
		</MainStyles>
	)
}

export default App

const MainStyles = styled.div`
	background: url(${image.bg3}) no-repeat top center / 100% auto #1f0f02;
	/* background: #1b0f01; */
	min-height: 100vh;
	/* color: #161616; */
	color: #fff;

	.hero {
		height: calc(100vh - 10vh);
		display: flex;
		justify-content: center;
		align-items: center;
		flex-direction: row;
		flex-wrap: wrap;
		&__left {
			width: 60%;
		}
		&__right {
			width: 40%;
			position: relative;
			img {
				width: 100%;
				animation: float 4s ease-in-out infinite;
			}
		}
		@media screen and (max-width: 740px) {
			flex-direction: column;
			&__left {
				width: 100%;
			}
			&__right {
				width: 100%;
			}
		}
	}
	.head {
		/* letter-spacing: 0.2em; */
		&__title {
			font-size: 5em;
			color: #fff;
			margin-bottom: 1%;
			line-height: 0.9;
			font-family: "Cantarell-Bold", sans-serif;
			animation: tracking-in-expand 0.7s cubic-bezier(0.215, 0.61, 0.355, 1)
				both;
			span {
				background: linear-gradient(90deg, #ed765e, #fea858);
				background-clip: text;
				-webkit-background-clip: text;
				-webkit-text-fill-color: transparent;
				margin-right: 2%;
			}
		}
		&__subtitle {
			font-size: 2vw;
			text-transform: uppercase;
		}
		&__btn {
		}
	}
	.copy {
		display: flex;
		margin-top: 2%;
		font-family: "Cantarell", sans-serif;

		input {
			width: 60%;
			padding: 1%;
			border-radius: 5px 0px 0px 5px;
			margin: 0;
			border: none;
			color: #ea8a09;
			font-family: "Cantarell", sans-serif;
			font-size: 1.3em;
			&::selection {
				background: transparent;
			}
		}

		&__btn {
			/* background: #ea8a09; */
			background: linear-gradient(90deg, #ed765e, #fea858);
			border: none;
			box-shadow: inset 0 1px 0 hsl(0deg 0% 100% / 15%),
				0 1px 1px rgb(49 49 49 / 8%);
			width: 15%;
			text-align: center;
			border-radius: 0px 5px 5px 0px;
			cursor: pointer;
			display: flex;
			align-items: center;
			justify-content: center;
			font-size: 1.3em;
			span {
			}
		}
	}
	.btngroup {
		margin-top: 3%;
		&__btn {
			cursor: pointer;
			border: 2px solid #ea8a09;
			border-radius: 10px;
			background: transparent;
			padding: 2%;
			transition: all 0.2s ease-in-out;
			color: #fff;
			&:hover {
				background: #ea8a09;
			}
			&:not(:last-child) {
				margin-right: 2%;
			}
		}
	}
	.social {
		position: absolute;
		bottom: 8%;
		width: 100%;
		&__icon {
			display: flex;
			justify-content: flex-start;
			align-items: center;
			a {
				width: 2%;
				&:not(:last-child) {
					margin-right: 1%;
				}
			}
			img {
				width: 100%;
			}
		}
		&__subtitle {
			text-transform: uppercase;
			margin-bottom: 1%;
		}
	}
	.bg {
		background: url(${image.bg4}) no-repeat top center / 100% auto #ffffff;
	}
	@keyframes float {
		0% {
			transform: translatey(0px);
		}
		50% {
			transform: translatey(-20px);
		}
		100% {
			transform: translatey(0px);
		}
	}
	@keyframes tracking-in-expand {
		0% {
			letter-spacing: -0.5em;
			opacity: 0;
		}
		40% {
			opacity: 0.6;
		}
		100% {
			opacity: 1;
		}
	}
`
