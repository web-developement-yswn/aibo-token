import React, { Component } from "react"
import styled from "styled-components"
// import { Card } from "@material-ui/core"

class MyCard extends Component {
	render() {
		const { title, tax, footer } = this.props
		return (
			<Card className="card">
				<div className="card__title bold">{title}</div>
				<div className="card__description bold">{tax}</div>
				<div className="card__footer bold">{footer}</div>
			</Card>
		)
	}
}
export default MyCard

const Card = styled.div`
	background: #fff;
	/* border: 0.1vw solid #251508; */
	box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.5);
	color: #251508;
	width: 30%;
	border-radius: 0.5vw;
	position: relative;
	overflow: hidden;
	.card {
		&__image {
			width: 10vw;
			background: #e98a09;
			position: absolute;
			height: 10vw;
			border-radius: 50%;
			top: -20%;
			left: 50%;
			transform: translateX(-50%);
		}
		&__title {
			font-size: 1.5em;
			color: #e98a09;
			background: linear-gradient(90deg, #ed765e, #fea858);
			background-clip: text;
			-webkit-background-clip: text;
			-webkit-text-fill-color: transparent;
		}
		&__description {
			font-size: 4em;
		}
		&__footer {
			background: linear-gradient(90deg, #ed765e, #fea858);
			color: #fff;
		}
	}
`
