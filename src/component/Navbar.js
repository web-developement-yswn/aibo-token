import React, { Component } from "react"
import styled from "styled-components"
import image from "../constant/ImportImages"

class Navbar extends Component {
	render() {
		return (
			<div className="container">
				<NavbarStyle className="menu">
					<div className="menu__left">
						<img src={image.logo} alt="" />
						<span>AIBO</span>
					</div>
					<ul className="menu__right">
						<li>
							<a href="#aa">Lorem</a>
						</li>
						<li>
							<a href="#aa">About us</a>
						</li>
						<li>
							<a href="#aa">White paper</a>
						</li>
						<li>
							<a href="#aa">Contact us</a>
						</li>
					</ul>
				</NavbarStyle>{" "}
			</div>
		)
	}
}
export default Navbar

const NavbarStyle = styled.div`
	position: sticky;
	top: 0;
	width: 100%;
	height: 10vh;
	display: flex;
	justify-content: space-between;
	align-items: center;
	padding: 1% 0;
	.menu {
		&__left {
			width: 60%;
			height: 100%;
			display: flex;
			align-items: center;
			img {
				height: 100%;
				width: auto;
				margin-right: 2%;
			}
			span {
				color: #e98a09;
				font-size: 1.5em;
				font-family: "Cantarell-Bold", sans-serif;
			}
		}
		&__right {
			text-align: center;
			width: 40%;
			display: flex;

			li {
				width: calc(100% / 4);
				padding: 0 3%;
				&:hover {
					color: #e98a09;
					/* border-bottom: 2px solid #ea8a09; */
				}
			}
		}
	}
`
