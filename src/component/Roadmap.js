import React, { Component } from "react"
import styled from "styled-components"

//component
import TaxCard from "./TaxCard"

class Tokenomic extends Component {
	render() {
		return (
			<TokenomicStyles>
				<div className="token container">
					<h2 className="token__title">Road map</h2>
					<div className="token__subtitle">
						Lorem ipsum dolor sit amet, consectetuer adipiscing elit
					</div>
					<div className="token__card">
						<TaxCard title="Liquidity Pool" tax="5%" footer="TAX to LP" />
						<TaxCard
							title="Allocate to All Holders"
							tax="3%"
							footer="TAX to LP"
						/>
						<TaxCard
							title="Expenses & Burn"
							tax="2%"
							footer="Expenses and Burn"
						/>
					</div>
				</div>
			</TokenomicStyles>
		)
	}
}
export default Tokenomic

const TokenomicStyles = styled.div`
	/* background: rgb(255, 255, 255); */
	text-align: center;
	color: #1f0f02;
	.token {
		padding: 7% 0%;
		&__title {
			font-family: "Cantarell-Bold", sans-serif;
			text-transform: uppercase;
			font-size: 3vw;
		}
		&__subtitle {
			font-size: 1.5vw;
			color: #705a4a;
		}
		&__card {
			width: 100%;
			margin-top: 4%;
			display: flex;
			justify-content: space-between;
			align-items: center;
			flex-wrap: wrap;
		}
	}
`
