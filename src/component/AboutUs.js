import React, { Component } from "react"
import styled from "styled-components"

//component
import Card from "./Card"

class AboutUs extends Component {
	render() {
		return (
			<AboutUsStyles>
				<div className="aboutus container">
					<h2 className="aboutus__title">About us</h2>
					<div className="aboutus__subtitle">
						Lorem ipsum dolor sit amet, consectetuer adipiscing elit
					</div>
					<div className="aboutus__card">
						<Card />
						<Card />
						<Card />
					</div>
				</div>
			</AboutUsStyles>
		)
	}
}
export default AboutUs

const AboutUsStyles = styled.div`
	/* background: rgb(255, 255, 255);
	background: linear-gradient(
		0deg,
		rgba(255, 255, 255, 1) 0%,
		rgba(113, 94, 76, 1) 50%,
		rgba(30, 14, 2, 1) 100%
	); */
	min-height: 100vh;
	text-align: center;
	.aboutus {
		padding: 7% 0%;
		&__title {
			font-family: "Cantarell-Bold", sans-serif;
			text-transform: uppercase;
			font-size: 3vw;
		}
		&__subtitle {
			font-size: 1.5vw;
			color: #705a4a;
		}
		&__card {
			width: 100%;
			margin-top: 4%;
			display: flex;
			justify-content: space-between;
			align-items: center;
			flex-wrap: wrap;
		}
	}
`
