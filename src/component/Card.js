import React, { Component } from "react"
import styled from "styled-components"
// import { Card } from "@material-ui/core"

class MyCard extends Component {
	render() {
		return (
			<Card className="card">
				<div className="card__image"></div>
				<div className="card__title bold">Lorem ipsum dolor sit amet</div>
				<div className="card__description">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque
					tempus arcu ac urna fringilla, sit amet fringilla arcu faucibus.
					Nullam tincidunt diam nec nibh lobortis, id eleifend felis
					condimentum. Ut sed urna quis nunc efficitur pellentesque. Phasellus
					molestie nec metus vestibulum scelerisque. Fusce porttitor sem quis
					nibh ultricies gravida.
				</div>
			</Card>
		)
	}
}
export default MyCard

const Card = styled.div`
	background: #fff;
	/* border: 0.1vw solid #251508; */
	box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.5);
	color: #251508;
	width: 30%;
	border-radius: 0.5vw;
	position: relative;
	padding: 3%;
	margin-top: 8%;
	.card {
		&__image {
			width: 10vw;
			/* background: #e98a09; */
			background: linear-gradient(90deg, #ed765e, #fea858);
			position: absolute;
			height: 10vw;
			border-radius: 50%;
			top: -20%;
			left: 50%;
			transform: translateX(-50%);
		}
		&__title {
			font-size: 2em;
			padding-top: 11%;
		}
		&__description {
			text-align: left;
			padding-top: 5%;
		}
	}
`
