import React, { Component } from "react"
import styled from "styled-components"

//component
import Card from "./Card"

class Stats extends Component {
	render() {
		return (
			<StatsStyles>
				<div className="stats container">
					<h2 className="stats__title">Stats</h2>
					<div className="stats__subtitle">
						Lorem ipsum dolor sit amet, consectetuer adipiscing elit
					</div>
				</div>
				<div className="stats__bg">
					<div className="stats__tab container">
						<div className="stats__box">
							<div className="stats__head bold">AIBO Holders</div>
							<div className="stats__number bold">658K+</div>
						</div>
						<div className="stats__box">
							<div className="stats__head bold">Market Cap</div>
							<div className="stats__number bold">$34M+</div>
						</div>
						<div className="stats__box">
							<div className="stats__head bold">Donated to Charity</div>
							<div className="stats__number bold">$19500</div>
						</div>
						<div className="stats__box">
							<div className="stats__head bold">AIBO Holders</div>
							<div className="stats__number bold">658K+</div>
						</div>
						<div className="stats__box">
							<div className="stats__head bold">AIBO Holders</div>
							<div className="stats__number bold">658K+</div>
						</div>
					</div>
				</div>
			</StatsStyles>
		)
	}
}
export default Stats

const StatsStyles = styled.div`
	/* background: rgb(255, 255, 255);
	background: linear-gradient(
		0deg,
		rgba(255, 255, 255, 1) 0%,
		rgba(113, 94, 76, 1) 50%,
		rgba(30, 14, 2, 1) 100%
	); */
	text-align: center;
	padding: 7% 0%;
	.stats {
		&__title {
			color: #1f0f02;
			text-transform: uppercase;
			font-size: 3vw;
		}
		&__subtitle {
			font-size: 1.5vw;
			color: #705a4a;
		}
		&__bg {
			margin-top: 4%;
			width: 100%;
			background: #ffeee3;
			padding: 3% 0%;
		}
		&__tab {
			display: flex;
			justify-content: space-between;
		}
		&__box {
		}
		&__head {
			color: #705a4a;
		}
		&__number {
			font-size: 2em;
			color: #241001;
		}
	}
`
