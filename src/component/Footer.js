import React, { Component } from "react"
import styled from "styled-components"
import image from "../constant/ImportImages"

class Footer extends Component {
	render() {
		return (
			<FooterStyles>
				<div className="footer container ">
					<div>Copyright © 2021, All Right Reserved. AIBO</div>
					<div className="fsocial">
						<div className="fsocial__icon">
							<a href="#aa" target="__blank">
								<img src={image.facebook} alt="fb" />
							</a>
							<a href="#aa" target="__blank">
								<img src={image.twitter} alt="tw" />
							</a>
							<a href="#aa" target="__blank">
								<img src={image.telegram} alt="tg" />
							</a>
						</div>
					</div>
				</div>
			</FooterStyles>
		)
	}
}
export default Footer

const FooterStyles = styled.div`
	/* background: rgb(255, 255, 255);
	background: linear-gradient(
		0deg,
		rgba(255, 255, 255, 1) 0%,
		rgba(113, 94, 76, 1) 50%,
		rgba(30, 14, 2, 1) 100%
	); */
	text-align: center;
	background: #ffeee3;
	color: #1f0f02;
	.footer {
		padding: 2% 0;
		&__social {
		}
	}
	.fsocial {
		width: 100%;
		margin-top: 1%;
		&__icon {
			display: flex;
			justify-content: center;
			align-items: center;
			a {
				width: 2%;
				&:not(:last-child) {
					margin-right: 1%;
				}
			}
			img {
				width: 100%;
			}
		}
		&__subtitle {
			text-transform: uppercase;
			margin-bottom: 1%;
		}
	}
`
